import { createGlobalStyle } from 'styled-components'
import themeStore from '../stores/Theme/ThemeStore'

const GlobalStyle = createGlobalStyle`
  html {
    font-size: 62.5%;
  }

  body {
    padding: 0;
    margin: 0;
    background-color: ${themeStore.theme.color.white};
    font-family: ${themeStore.theme.font.family};
    font-size: ${themeStore.theme.font.size};
    font-weight: ${themeStore.theme.font.weight};
    font-style: ${themeStore.theme.font.style};
    font-variant: initial;
  }
`

export default GlobalStyle
