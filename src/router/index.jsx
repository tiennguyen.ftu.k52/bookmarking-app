import React, { Fragment } from 'react'
import { Route, Router, Switch } from 'react-router-dom'
import { history } from '../stores/Routing/RoutingStore'
import GlobalStyle from '../ui/global'

import Bookmark from '../components/Bookmark/Bookmark'

const router = () => (
  <Fragment>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Bookmark} />
      </Switch>
    </Router>
    <GlobalStyle />
  </Fragment>
)

export default router
