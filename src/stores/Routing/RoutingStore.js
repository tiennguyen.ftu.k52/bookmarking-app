import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import createHashHistory from 'history/createHashHistory'

const hashHistory = createHashHistory()
const routingStore = new RouterStore()

export const history = syncHistoryWithStore(hashHistory, routingStore)

export default routingStore
