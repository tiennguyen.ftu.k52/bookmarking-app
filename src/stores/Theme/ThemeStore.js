import { computed, observable } from 'mobx'

export class ThemeStore {
  @computed get theme() {
    return {
      font: this.font,
      color: this.color,
    }
  }

  @observable font = {
    family: 'Circular Std Book, -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif',
    size: '1.6rem',
    weight: 'normal',
    style: 'normal',
  }

  @observable color = {
    black: '#000',
    white: '#fff',
  }
}

export default new ThemeStore()
