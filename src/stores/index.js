import routingStore from './Routing/RoutingStore'
import themeStore from './Theme/ThemeStore'

const stores = {
  routingStore,
  themeStore,
}

export default stores
