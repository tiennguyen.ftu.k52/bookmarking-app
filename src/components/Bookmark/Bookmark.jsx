import React from 'react'
import { observer } from 'mobx-react'

@observer
class Bookmark extends React.Component {
  render() {
    return (<div>
      <h2>Welcome to Bookmarking App!</h2>
      <p>This is the description</p>
    </div>)
  }
}

export default Bookmark

