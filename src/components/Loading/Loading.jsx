import React from 'react'
import styled from 'styled-components'
import { Spin } from '../../ui/antd'

export const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  height: 100vh;
  width: 100vw;
  z-index: 1000;
  background: transparent;
`

export const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(0, 0, 0, 0.5);
`

const Loading = () => (
  <Overlay>
    <Container>
      <Spin size="large" />
    </Container>
  </Overlay>
)

export default Loading

